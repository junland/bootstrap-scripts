#!/bin/true
set -e

. $(dirname $(realpath -s $0))/common.sh

extractSource coreutils
serpentChrootCd coreutils-*
pushd coreutils-*


printInfo "Configuring coreutils"
export FORCE_UNSAFE_CONFIGURE=1
serpentChroot ./configure \
    --build="${SERPENT_TRIPLET}" \
    --host="${SERPENT_TRIPLET}" \
    --enable-largefile \
    --prefix=/usr \
    --sysconfdir=/etc \
    --libdir=/usr/lib \
    --bindir=/usr/bin \
    --sbindir=/usr/sbin \
    --datadir=/usr/share \
    --enable-single-binary


printInfo "Building coreutils"
serpentChroot make -j "${SERPENT_BUILD_JOBS}"

printInfo "Installing coreutils"
serpentChroot make -j "${SERPENT_BUILD_JOBS}" install
